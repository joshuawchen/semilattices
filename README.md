# README #

This package implements a lightweight library for semilattices in python.

See the [documentation](https://semilattices.readthedocs.io/en/latest/) for more details.

Submit issues [here](https://bitbucket.org/joshuawchen/semilattices/issues)
