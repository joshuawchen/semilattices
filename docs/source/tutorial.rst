.. _tutorial:

Tutorial
========

Let us first set the generic notation and definitions common to all the semilattices
inheriting from :class:`CoordinateSemilattice<semilattices.CoordinateSemilattice>`.
Additional notations and definitions will be introduced as needed for the particular
semilattices presented.

The tuple :math:`\mathcal{S}=(\mathcal{V}, \mathcal{E})`
is a **coordinate semilattice** of dimension :math:`d` if

* :math:`\mathcal{S}` is *connected*
* there exist a unique vertex :math:`r\in\mathcal{V}`, denoted **root**, such that
  :math:`r.\text{parents} = \varnothing`
* each vertex :math:`v\in\mathcal{V}` has at most :math:`d` **children**
* each vertex :math:`v\in\mathcal{V}` which is :math:`v.\text{level}` edges away from the root
  has at most :math:`\min(d, v.\text{level})` **parents** (the connectivity properties implies
  that any vertex but the root must have at least one parent and up to `d` parents).

.. toctree::
   :hidden:
   :maxdepth: 2
   
   CoordinateSemilattice.ipynb
   DecreasingCoordinateSemilattice.ipynb
   SortedCoordinateSemilattice.ipynb
