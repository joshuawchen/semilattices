.. semilattices documentation master file, created by
   sphinx-quickstart on Thu Oct 18 11:14:24 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Semilattices
========================================

This package is devoted to the construction and modification of semilattices. Mathematically, a lower-(upper-) semilattice is a partially ordered set for which any nonempty finite subset has an infimum(supremum). The greatest lower bound (least upper bound) of a lower-(upper-) semilattice is called is meet (join).

In the language of graphs, a semilattice is a special type of rooted connected graph.
In particular it can be viewed as a **tree whose leaves can have multiple parents**.

The package implements the interface to many types of semilattices,
which enforce different connectivity properties and provide access to
different data structures.

The most basic type of practically useful semilattice is the
:class:`CoordinateSemilattice<semilattices.CoordinateSemilattice>`,
which can be defined as follows: the tuple
:math:`\mathcal{S}=(\mathcal{V}, \mathcal{E})` is a **coordinate semilattice** of
dimension :math:`d` if

* :math:`\mathcal{S}` is *connected*
* there exist a unique vertex :math:`r\in\mathcal{V}`, denoted **root**, such that
  :math:`r.\text{parents} = \varnothing`
* each vertex :math:`v\in\mathcal{V}` has at most :math:`d` **children**
* each vertex :math:`v\in\mathcal{V}` which is :math:`v.\text{level}` edges away from the root
  has at most :math:`\min(d, v.\text{level})` **parents** (the connectivity properties implies
  that any vertex but the root must have at least one parent and up to `d` parents).
  
The children of a vertex of level :math:`v.\text{level}`
will belong to the level :math:`v.\text{level}+1`
while its parents will belong to the level :math:`v.\text{level}-1`.

The following figure shows a :math:`d=3` dimensional semilattice, where
each vertex is labeled using its **coordinates** :math:`v.\text{coordinates}`,
namely the number of edges in each coordinate direction which connect :math:`v` to the
root. The root can be seen at the bottom to have an empty coordinates set.
Its children have coordinate :math:`1` in the direction in which they are related
to the root, and so on. The vertices in the same level are displayed along the same row.
Vertices have a **level** property :math:`v.\text{level}:=\sum v.\text{coordinates}`.

.. image:: figs/intro-semilattice.svg
   :height: 200px
   :width: 430px
   :align: center

The :ref:`tutorial` will walk the user through the construction and handling
of :class:`CoordinateSemilattice<semilattices.CoordinateSemilattice>` and
semilattice classes that extend it.

.. toctree::
   :hidden:
   :maxdepth: 1

   install
   tutorial
   api-semilattices
   credits


