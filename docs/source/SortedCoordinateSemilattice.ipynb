{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import semilattices as SL\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Sorted semilattices\n",
    "\n",
    "A sorted semilattice is a semilattice $\\mathcal{S}$  in which each vertex $v \\in \\mathcal{S}$ can be assigned one or more **labels** $v.\\text{labels}$ defining a **total ordering** on $\\mathcal{S}$ if they are unique. Not all the vertices *need* labels, some may be unlabeled, in which case they will be ordered according to their coordinates after all the vertices with a label."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Single sorting label\n",
    "\n",
    "We start borrowing an example from the section on [coordinate semilattice](CoordinateSemilattice.html) where now we allow for vertices with a label \"weight\" and assign it a value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl = SL.SortedCoordinateSemilattice(dims=2, label_keys=['weight']) # Specify the sorting labels\n",
    "root = sl.new_vertex(labels={'weight':10})   \n",
    "c1 = sl.new_vertex(edge=1, parent=root, labels={'weight':9})\n",
    "c0 = sl.new_vertex(edge=0, parent=root, labels={'weight':8})\n",
    "c0c0 = sl.new_vertex(edge=0, parent=c0, labels={'weight':7})\n",
    "c0c1 = sl.new_vertex(edge=1, parent=c0, labels={'weight':6})\n",
    "c0c1c1 = sl.new_vertex(edge=1, parent=c0c1, labels={'weight':5})\n",
    "c0c1c0 = sl.new_vertex(edge=0, parent=c0c1, labels={'weight':4})\n",
    "c0c0c0 = sl.new_vertex(edge=0, parent=c0c0, labels={'weight':3})\n",
    "c0c0c0c0 = sl.new_vertex(edge=0, parent=c0c0c0, labels={'weight':2})\n",
    "c0c0c0c1 = sl.new_vertex(edge=1, parent=c0c0c0, labels={'weight':1})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now visualize the semilattice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, show=False, color='label', color_label='weight')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, the labels can be set afterward using the function ``update_labels``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, v in enumerate(sl): sl.update_labels( v, weight=len(sl)-i )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can access the vertices now in the order sorted by the \"weight\" using the ``get_sorted`` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in sl.vertices.get_sorted('weight'):\n",
    "    print(str(v.coordinates) + \": %d\" % v.labels.get('weight'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since the semilattice has only one label, one can use directly the ``sorted`` property."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in sl.vertices.sorted:\n",
    "    print(str(v.coordinates) + \": %d\" % v.labels.get('weight'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can also access the sorted subset of vertices in the frontier."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in sl.frontier.sorted:\n",
    "    print(str(v.coordinates) + \": %d\" % v.labels.get('weight'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Labels can be removed from single vertices through the ``update_labels`` functions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.update_labels(root, weight=None)\n",
    "for v in sl:\n",
    "    lbl = str(v.labels.get('weight'))\n",
    "    print(str(v.coordinates) + \": \" + lbl)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Multiple sorting labels\n",
    "\n",
    "We use again the previous coordinate semilattice here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl = SL.SortedCoordinateSemilattice(dims=2, label_keys=['weight', 'degree']) \n",
    "root = sl.new_vertex()   \n",
    "c1 = sl.new_vertex(edge=1, parent=root)\n",
    "c0 = sl.new_vertex(edge=0, parent=root)\n",
    "c0c0 = sl.new_vertex(edge=0, parent=c0)\n",
    "c0c1 = sl.new_vertex(edge=1, parent=c0)\n",
    "c0c1c1 = sl.new_vertex(edge=1, parent=c0c1)\n",
    "c0c1c0 = sl.new_vertex(edge=0, parent=c0c1)\n",
    "c0c0c0 = sl.new_vertex(edge=0, parent=c0c0)\n",
    "c0c0c0c0 = sl.new_vertex(edge=0, parent=c0c0c0)\n",
    "c0c0c0c1 = sl.new_vertex(edge=1, parent=c0c0c0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As before we assign labels to all the vertices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, v in enumerate(sl): sl.update_labels( v, weight=len(sl)-i, degree=v.coordinates.lp(p=1) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And visualize both."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, show=False, color='label', color_label='weight')\n",
    "sl.draw(cartesian=True, show=False, color='label', color_label='degree')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Missing labels\n",
    "\n",
    "Here we consider the case only a subset of the vertices are assigned a label. For example only the node in the frontier are assigned a \"degree\"."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl = SL.SortedCoordinateSemilattice(dims=2, label_keys=['degree']) # Specify the sorting labels\n",
    "root = sl.new_vertex()   \n",
    "c1 = sl.new_vertex(edge=1, parent=root, labels={'degree': 10})\n",
    "c0 = sl.new_vertex(edge=0, parent=root, labels={'degree': 1})\n",
    "c0c0 = sl.new_vertex(edge=0, parent=c0, labels={'degree': 1})\n",
    "c0c1 = sl.new_vertex(edge=1, parent=c0, labels={'degree': 1})\n",
    "c0c1c1 = sl.new_vertex(edge=1, parent=c0c1, labels={'degree': 1})\n",
    "c0c1c0 = sl.new_vertex(edge=0, parent=c0c1, labels={'degree': 1})\n",
    "c0c0c0 = sl.new_vertex(edge=0, parent=c0c0, labels={'degree': 1})\n",
    "c0c0c0c0 = sl.new_vertex(edge=0, parent=c0c0c0, labels={'degree': 1})\n",
    "c0c0c0c1 = sl.new_vertex(edge=1, parent=c0c0c0, labels={'degree': 1})"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in sl.frontier: sl.update_labels( v, degree=v.coordinates.lp(p=1) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The nodes with no label will be color-coded to black here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, show=False, color='label', color_label='degree')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The labels with no assigned values raise a ``KeyError`` meaning that the corresponding key is unassigned."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in sl.vertices:\n",
    "    try:\n",
    "        print(str(v.coordinates) + \": %d\" % v.labels['degree'])\n",
    "    except KeyError:\n",
    "        print(str(v.coordinates) + \": missing 'degree' label \")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Automatic creation of decreasing sorted semilattices\n",
    "\n",
    "Here we create a [decreasing coordinate semilattice](DecreasingCoordinateSemilattice.html) with multiple labels: one expressing the $l_p$ norm of the vertices ($p=0.4$) and another random."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "sl = SL.create_lp_semilattice(\n",
    "    2, 15, p=.45, weights={0:1.0,1:1.4},\n",
    "    SemilatticeConstructor=SL.SortedDecreasingCoordinateSemilattice,\n",
    "    label_keys = ['weight', 'random'])\n",
    "for v in sl: sl.update_labels(\n",
    "        v,\n",
    "        weight=v.coordinates.lp(p=.4),\n",
    "        random=np.random.randn())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And let us visualize them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, show=False, mark_frontier=False, mark_childless=True, mark_admissible_frontier=True, color='label', color_label='weight')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, show=False, color='label', color_label='random', nodes_cmap='jet')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
