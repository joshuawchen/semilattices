{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import semilattices as SL\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Coordinate semilattices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We start by constructing a semilattice, where we manually keep track of the nodes created. Note, it is not required to assign a new vertex (the return of the method call ``sl.new_vertex``) to a variable, but this becomes useful for manual defining the graph dependence structure of the semilattice, as we do throughout this tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl = SL.CoordinateSemilattice(dims=2)     # Creates a semilattice of dimension 2\n",
    "print(sl.properties)\n",
    "root = sl.new_vertex()                    # Add the root vertex to the semilattice\n",
    "c1 = sl.new_vertex(edge=1, parent=root)   # Add the child along coordinate 1 to the root vertex"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next we plot the current semilattice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, let us **add** some new nodes..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c0 = sl.new_vertex(edge=0, parent=root)   # Add the child along coordinate 0 to the root vertex\n",
    "c0c0 = sl.new_vertex(edge=0, parent=c0)   # Add the child along coordinate 0 to the c0 vertex\n",
    "c0c1 = sl.new_vertex(edge=1, parent=c0)   # Add the child along coordinate 1 to the c0 node\n",
    "sl.draw(cartesian=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that in line 3 above, we add the child of ``c0`` along coordinate 1. This child not only has ``c0`` as its parent, but also the vertex ``c1`` which was inserted previously. The function [new_vertex](api-semilattices.html#Semilattice.new_vertex) detects the fact that the parent ``c1`` exists and connects it correctly.\n",
    "\n",
    "Let us add some more vertices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c0c1c1 = sl.new_vertex(edge=1, parent=c0c1)\n",
    "c0c1c0 = sl.new_vertex(edge=0, parent=c0c1)\n",
    "c0c0c0 = sl.new_vertex(edge=0, parent=c0c0)\n",
    "c0c0c0c0 = sl.new_vertex(edge=0, parent=c0c0c0)\n",
    "c0c0c0c1 = sl.new_vertex(edge=1, parent=c0c0c0)\n",
    "sl.draw(cartesian=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Adding vertices that already exist raises exceptions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    c0c0c0 = sl.new_vertex(edge=0, parent=c0c0)\n",
    "except SL.ChildAlreadyExists:\n",
    "    print(\"Catched exception of child already existing...\")\n",
    "sl.draw(cartesian=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible to **remove** nodes, while preserving the connectivity of the semilattice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.delete_vertex(c0c0)\n",
    "sl.draw(cartesian=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The frontier\n",
    "\n",
    "The frontier is the subset of vertices $\\mathcal{F} \\subset \\mathcal{V}$ that admit additional children. As we state in the definition of coordinate semilattice, every vertex can have at most $d$ children. Therefore a vertex is in the frontier if it has less than $d$ children.\n",
    "\n",
    "The frontier can be accessed by the following field."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.frontier"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We may iterate over the frontier and print out the coordinates of the vertices belonging to it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in sl.frontier:\n",
    "    print(v.coordinates)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or, we could visualize it using different markers (squares for the nodes in the frontier)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, mark_frontier=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's delete a vertex!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.delete_vertex(c1)\n",
    "sl.draw(cartesian=True, mark_frontier=True, show=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in sl.frontier:\n",
    "    print(v.coordinates)\n",
    "print(\"or another way:\")\n",
    "for v in SL.LevelsIterable(sl, iter_attribute='_l1_frontier_partition'):\n",
    "    print(v.coordinates)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for lvl, vertices in sl.l1_frontier_partition.items():\n",
    "    print(\"lvl\", lvl)\n",
    "    for v in vertices:\n",
    "        print(v.coordinates)\n",
    "    print(\"\\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Childless vertices\n",
    "\n",
    "Childless vertices are the subset of vertices $\\mathcal{F} \\subset \\mathcal{V}$ that have no children. This set is a subset of the frontier.\n",
    "\n",
    "The childless vertices can be accessed by the following field."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.childless"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As with the frontier, we may iterate over the childless vertices and print out the coordinates of the vertices belonging to it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in sl.childless:\n",
    "    print(v.coordinates)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or, as with the frontier, we could visualize it using different markers (♢ for the nodes in the childless set). Note, if one also marks the frontier, the childless vertices will default to marking with ♢, even though they are a subset of the frontier as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, mark_frontier=True, mark_childless=True,  show=False) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The levels structure\n",
    "\n",
    "The sets $\\{\\mathcal{L}_i\\}$ of $\\mathcal{V}$ partition $\\mathcal{V}$ in into vertices that belong to level $i$. These correspond to vertices whose coordinates sum to $i$, or in other words whose $\\ell^1$-norm of the coordinates is $i$.\n",
    "\n",
    "These sets can be accessed through the following data structure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.l1_vertices_partition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can for example access all the vertices in level $1$ as follows."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in sl.l1_vertices_partition[1]:\n",
    "    print(v.coordinates)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or we could visualize the semilattice with vertices colored according to their l1 norm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, color='l1_norm', show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also visualize the semilattice with vertices colored according to their 'average dimension.'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, color='dims', show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Iterating over a semilattice\n",
    "\n",
    "There are a number of ways for iterating over a semilattice. The simplest way is using the semilattice itself, which does not guarantee any specific ordering."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in sl:\n",
    "    print(v.coordinates)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively one may be interested in iterating **breadth first**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in SL.BreadthFirstSemilatticeIterable(sl):\n",
    "    print(v.coordinates)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Or **depth first**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in SL.DepthFirstSemilatticeIterable(sl):\n",
    "    print(v.coordinates)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One could also to iterate **level-by-level**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in SL.LevelsIterable(sl):\n",
    "    print(v.coordinates)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Union and intersection of semilattices\n",
    "\n",
    "Semilattices are closed under union and intersection operations. So, two semilattices can be merged into one by taking the union or the intersection of the two. These operations could be performed in-place or not-in-place.\n",
    "\n",
    "In order to show this feature, let us first generate another semilattice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl2 = SL.CoordinateSemilattice(dims=2)\n",
    "root = sl2.new_vertex()\n",
    "c0 = sl2.new_vertex(edge=0, parent=root)\n",
    "c1 = sl2.new_vertex(edge=1, parent=root)\n",
    "c0c1 = sl2.new_vertex(edge=0, parent=c1)\n",
    "c1c1 = sl2.new_vertex(edge=1, parent=c1)\n",
    "c1c1c1 = sl2.new_vertex(edge=1, parent=c1c1)\n",
    "c1c1c1c0 = sl2.new_vertex(edge=0, parent=c1c1c1)\n",
    "c0c0 = sl2.new_vertex(edge=0, parent=c0)\n",
    "sl2.draw(cartesian=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then let's take the **not-in-place union** with the previous semilattice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "usl = sl | sl2\n",
    "usl.draw(cartesian=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The corresponding **in-place** operation is performed as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl_copy = sl.copy()    # We copy sl in order to be able to re-use it later\n",
    "sl_copy |= sl2\n",
    "sl_copy.draw(cartesian=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us take the **not-in-place intersection** with the previous semilattice."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "isl = sl & sl2\n",
    "isl.draw(cartesian=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The corresponding **in-place** operation is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl_copy = sl.copy()    # We copy sl in order to be able to re-use it later\n",
    "sl_copy &= sl2\n",
    "sl_copy.draw(cartesian=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualization\n",
    "\n",
    "In the previous examples we have seen several usages of the function [CoordinateSemilattice.draw](api-semilattices.html#CoordinateSemilattice.draw). There are two main plotting projections available, the cartesian, which works in 2d and 3d, and the \"default\" which project everything in 2 dimensions.\n",
    "\n",
    "We construct a 3 dimensional semilattice to show their differences."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl = SL.CoordinateSemilattice(dims=3)\n",
    "root = sl.new_vertex()\n",
    "c0 = sl.new_vertex(edge=0, parent=root)\n",
    "c1 = sl.new_vertex(edge=1, parent=root)\n",
    "c2 = sl.new_vertex(edge=2, parent=root)\n",
    "c0c0 = sl.new_vertex(edge=0, parent=c0)\n",
    "c0c1 = sl.new_vertex(edge=1, parent=c0)\n",
    "c0c2 = sl.new_vertex(edge=2, parent=c0)\n",
    "c0c0c0 = sl.new_vertex(edge=0, parent=c0c0)\n",
    "c0c0c1 = sl.new_vertex(edge=1, parent=c0c0)\n",
    "c1c2 = sl.new_vertex(edge=2, parent=c1)\n",
    "c0c1c2 = sl.new_vertex(edge=2, parent=c0c1)\n",
    "c1c1 = sl.new_vertex(edge=1, parent=c1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In general the cartesian projection provides a better spacing of the nodes, but can be used only in 2 dimensions or 3 dimensions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The \"default\" projection instead puts the different axis of the dimensions along different branches of the semilattice projection and, as a result of plotting a high-dimensional semilattice in 2 dimesnions, clusters the nodes towards these branches."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a result some nodes and some edges may be crossing and overlapping, which is an unavoidable side effect. On the other hand this projection is still useful to get a general overview of the semilattice in high-dimensions."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
