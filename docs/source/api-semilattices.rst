API - semilattices
==================

**Classes**


.. inheritance-diagram:: semilattices.ComplementSparseKeysSet semilattices.CoordinateDict semilattices.DefaultDict semilattices.LevelsPartition semilattices.MixedElement semilattices.MixedSortedContainer semilattices.SpaceWeightDict semilattices.StaticElement semilattices.ArgumentsException semilattices.ChildAlreadyExists semilattices.CorruptedSemilatticeException semilattices.DecreasingSemilatticeException semilattices.EdgeException semilattices.EmptySemilatticeException semilattices.FrontierException semilattices.GraphException semilattices.InitializationException semilattices.InvalidChild semilattices.InvalidDimension semilattices.InvalidParent semilattices.InvalidVertex semilattices.InvalidVertexConstructor semilattices.IteratorException semilattices.LabelsException semilattices.SemilatticeException semilattices.SparseKeysException semilattices.ViolatesDecreasingProperty semilattices.VertexException semilattices.SemilatticeIterable semilattices.BreadthFirstSemilatticeIterable semilattices.DepthFirstSemilatticeIterable semilattices.CoupledSemilatticeIterable semilattices.CoupledIntersectionSemilatticeIterable semilattices.BreadthFirstCoupledIntersectionSemilatticeIterable semilattices.DepthFirstCoupledIntersectionSemilatticeIterable semilattices.CoupledUnionSemilatticeIterable semilattices.BreadthFirstCoupledUnionSemilatticeIterable semilattices.DepthFirstCoupledUnionSemilatticeIterable semilattices.LevelsIterable semilattices.ParentsPowersetIterable semilattices.SLO semilattices.SemilatticeVertex semilattices.SparseVertex semilattices.CoordinateVertex semilattices.LabeledVertex semilattices.LabeledSparseVertex semilattices.SparseCoordinateVertex semilattices.LabeledCoordinateVertex semilattices.SparseLabeledCoordinateVertex semilattices.Semilattice semilattices.CoordinateSemilattice semilattices.DecreasingCoordinateSemilattice semilattices.SortedCoordinateSemilattice semilattices.SortedDecreasingCoordinateSemilattice
   :parts: 1


==============================================================================================================================================  ===================================================================================================================================================================================================
Class                                                                                                                                           Description
==============================================================================================================================================  ===================================================================================================================================================================================================
`ComplementSparseKeysSet <api-semilattices.html\#semilattices.ComplementSparseKeysSet>`_                                                        This is used to handle the ``sparse_keys`` (admissible children) of the root.
`CoordinateDict <api-semilattices.html\#semilattices.CoordinateDict>`_                                                                          Sorted dictionary of coordinates. It defaults to zero for missing elements.
`DefaultDict <api-semilattices.html\#semilattices.DefaultDict>`_                                                                                This is a dictionary which defaults to a prescribed value if a key is missing.
`LevelsPartition <api-semilattices.html\#semilattices.LevelsPartition>`_                                                                        Dictionary for keeping track of subsets of a coordinate semilattice at each level.
`MixedElement <api-semilattices.html\#semilattices.MixedElement>`_                                                                              An element whose comparability can change.
`MixedSortedContainer <api-semilattices.html\#semilattices.MixedSortedContainer>`_                                                              This is a container where some elements are sorted if assigned a label(s), otherwise are just stored.
`SpaceWeightDict <api-semilattices.html\#semilattices.SpaceWeightDict>`_                                                                        This is a dictionary that default to 1 for missing elements.
`StaticElement <api-semilattices.html\#semilattices.StaticElement>`_                                                                            An element whose comparability cannot change.
`ArgumentsException <api-semilattices.html\#semilattices.ArgumentsException>`_
`ChildAlreadyExists <api-semilattices.html\#semilattices.ChildAlreadyExists>`_
`CorruptedSemilatticeException <api-semilattices.html\#semilattices.CorruptedSemilatticeException>`_
`DecreasingSemilatticeException <api-semilattices.html\#semilattices.DecreasingSemilatticeException>`_
`EdgeException <api-semilattices.html\#semilattices.EdgeException>`_
`EmptySemilatticeException <api-semilattices.html\#semilattices.EmptySemilatticeException>`_
`FrontierException <api-semilattices.html\#semilattices.FrontierException>`_
`GraphException <api-semilattices.html\#semilattices.GraphException>`_
`InitializationException <api-semilattices.html\#semilattices.InitializationException>`_
`InvalidChild <api-semilattices.html\#semilattices.InvalidChild>`_
`InvalidDimension <api-semilattices.html\#semilattices.InvalidDimension>`_
`InvalidParent <api-semilattices.html\#semilattices.InvalidParent>`_
`InvalidVertex <api-semilattices.html\#semilattices.InvalidVertex>`_
`InvalidVertexConstructor <api-semilattices.html\#semilattices.InvalidVertexConstructor>`_
`IteratorException <api-semilattices.html\#semilattices.IteratorException>`_
`LabelsException <api-semilattices.html\#semilattices.LabelsException>`_
`SemilatticeException <api-semilattices.html\#semilattices.SemilatticeException>`_
`SparseKeysException <api-semilattices.html\#semilattices.SparseKeysException>`_
`ViolatesDecreasingProperty <api-semilattices.html\#semilattices.ViolatesDecreasingProperty>`_
`VertexException <api-semilattices.html\#semilattices.VertexException>`_
`SemilatticeIterable <api-semilattices.html\#semilattices.SemilatticeIterable>`_                                                                Base class defining an iterable for the semilattice.
`BreadthFirstSemilatticeIterable <api-semilattices.html\#semilattices.BreadthFirstSemilatticeIterable>`_                                        Breadth first iterable for the semilattice.
`DepthFirstSemilatticeIterable <api-semilattices.html\#semilattices.DepthFirstSemilatticeIterable>`_                                            Depth first iterable for the semilattice.
`CoupledSemilatticeIterable <api-semilattices.html\#semilattices.CoupledSemilatticeIterable>`_                                                  Base class defining an iterable for two semilattices at the same time.
`CoupledIntersectionSemilatticeIterable <api-semilattices.html\#semilattices.CoupledIntersectionSemilatticeIterable>`_                          Iterable over the intersection of two semilattices.
`BreadthFirstCoupledIntersectionSemilatticeIterable <api-semilattices.html\#semilattices.BreadthFirstCoupledIntersectionSemilatticeIterable>`_  Breadth first iterable over the intersection of two semilattices.
`DepthFirstCoupledIntersectionSemilatticeIterable <api-semilattices.html\#semilattices.DepthFirstCoupledIntersectionSemilatticeIterable>`_      Depth first iterable over the intersection of two semilattices.
`CoupledUnionSemilatticeIterable <api-semilattices.html\#semilattices.CoupledUnionSemilatticeIterable>`_                                        Iterable over the union of two semilattices.
`BreadthFirstCoupledUnionSemilatticeIterable <api-semilattices.html\#semilattices.BreadthFirstCoupledUnionSemilatticeIterable>`_                Iterable over the union of two semilattices.
`DepthFirstCoupledUnionSemilatticeIterable <api-semilattices.html\#semilattices.DepthFirstCoupledUnionSemilatticeIterable>`_                    Iterable over the union of two semilattices.
`LevelsIterable <api-semilattices.html\#semilattices.LevelsIterable>`_                                                                          Iterable over the semilattice vertices by level.
`ParentsPowersetIterable <api-semilattices.html\#semilattices.ParentsPowersetIterable>`_                                                        Iterable over the vertices that are the ancestors defined by the powerset of the parent directions of v.
`SLO <api-semilattices.html\#semilattices.SLO>`_                                                                                                Base object for every object in the module.
`SemilatticeVertex <api-semilattices.html\#semilattices.SemilatticeVertex>`_                                                                    A vertex that can have multiple parents.
`SparseVertex <api-semilattices.html\#semilattices.SparseVertex>`_                                                                              A vertex that admit children only in a prescribed set of directions.
`CoordinateVertex <api-semilattices.html\#semilattices.CoordinateVertex>`_                                                                      A semilattice vertex that have the concept of directional distance from the root.
`LabeledVertex <api-semilattices.html\#semilattices.LabeledVertex>`_                                                                            Labeled Vertices are provided an optional label(s).
`LabeledSparseVertex <api-semilattices.html\#semilattices.LabeledSparseVertex>`_                                                                A vertex that is :class:`LabeledVertex` and :class:`SparseVertex`.
`SparseCoordinateVertex <api-semilattices.html\#semilattices.SparseCoordinateVertex>`_                                                          A vertex that is :class:`CoordinateVertex` and :class:`SparseVertex`.
`LabeledCoordinateVertex <api-semilattices.html\#semilattices.LabeledCoordinateVertex>`_                                                        A vertex that is :class:`LabeledVertex` and :class:`CoordinateVertex`.
`SparseLabeledCoordinateVertex <api-semilattices.html\#semilattices.SparseLabeledCoordinateVertex>`_                                            A vertex that is :class:`LabeledVertex`, :class:`CoordinateVertex` and :class:`SparseVertex`.
`Semilattice <api-semilattices.html\#semilattices.Semilattice>`_                                                                                An (order-) semilattice is a rooted connected directed acyclic graph resembling the structure of a tree, i.e. it has a depth/levels. However, unlike a tree, each vertex can have multiple parents.
`CoordinateSemilattice <api-semilattices.html\#semilattices.CoordinateSemilattice>`_                                                            A semilattice with pre-defined dimension.
`DecreasingCoordinateSemilattice <api-semilattices.html\#semilattices.DecreasingCoordinateSemilattice>`_                                        A DecreasingCoordinateSemilattice is a semilattice :math:`(\mathcal{S}\,\le)` that is closed under meets,
`SortedCoordinateSemilattice <api-semilattices.html\#semilattices.SortedCoordinateSemilattice>`_                                                A SortedCoordinateSemilattice is a semilattice with Labeled vertices.
`SortedDecreasingCoordinateSemilattice <api-semilattices.html\#semilattices.SortedDecreasingCoordinateSemilattice>`_
==============================================================================================================================================  ===================================================================================================================================================================================================


**Functions**

==============================================================================================  =======================================================================================================
Function                                                                                        Description
==============================================================================================  =======================================================================================================
`adjacency_mat_eq <api-semilattices.html\#semilattices.adjacency_mat_eq>`_                      Checks equality between two adjacency matrices
`adjacency_mat <api-semilattices.html\#semilattices.adjacency_mat>`_                            Creates an adjacency matrix defining the directed edges (in or out) between vertices and other_vertices
`setLogLevel <api-semilattices.html\#semilattices.setLogLevel>`_                                Set the log level for all existing and new objects related to the semilattices module
`deprecate <api-semilattices.html\#semilattices.deprecate>`_
`cprofile <api-semilattices.html\#semilattices.cprofile>`_
`any_kwarg_required <api-semilattices.html\#semilattices.any_kwarg_required>`_
`default_kwargs <api-semilattices.html\#semilattices.default_kwargs>`_
`exactly_one_kwarg_optional <api-semilattices.html\#semilattices.exactly_one_kwarg_optional>`_
`exactly_one_kwarg_required <api-semilattices.html\#semilattices.exactly_one_kwarg_required>`_
`invalid_type <api-semilattices.html\#semilattices.invalid_type>`_
`optional_kwargs_types <api-semilattices.html\#semilattices.optional_kwargs_types>`_
`require_kwargs <api-semilattices.html\#semilattices.require_kwargs>`_
`required_kwargs <api-semilattices.html\#semilattices.required_kwargs>`_
`valid_type <api-semilattices.html\#semilattices.valid_type>`_
`argsort <api-semilattices.html\#semilattices.argsort>`_
`create_lp_semilattice <api-semilattices.html\#semilattices.create_lp_semilattice>`_            Create the semilattice of all vertices in the :math:`\ell^p` spherical sector of a given norm
`permute <api-semilattices.html\#semilattices.permute>`_                                        Creates a new semilattice with permuted dimensions
==============================================================================================  =======================================================================================================

**Documentation**

.. automodule:: semilattices
   :members:

