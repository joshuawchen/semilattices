Installation
============

Supported systems
-----------------

* \*nix like OS (Linux, Unix, ...)
* Mac OS

Other operating systems have not been tested and they likely require a more complex procedure for the installation (this includes the Microsoft Windows family..).

We recommend to work in a virtual environment using `virtualenv <https://virtualenv.readthedocs.io/en/latest/>`_ or `Anaconda <https://www.continuum.io/why-anaconda>`_.

Installation requirements
-------------------------

* `python2 <https://www.python.org/>`_ or `python3 <https://www.python.org/>`_

.. toggle-header::
   :header: Installing requirements on Ubuntu 

     with `python2 <https://www.python.org/>`_

     .. prompt:: bash

        sudo apt-get install python

     or `python3 <https://www.python.org/>`_

     .. prompt:: bash

        sudo apt-get install python3

.. toggle-header:: 
   :header: Installing requirements on Mac OS (using homebrew)

     with `python2 <https://www.python.org/>`_

     .. prompt:: bash

        brew install python

     or `python3 <https://www.python.org/>`_

     .. prompt:: bash

        brew install python3

Installation
------------

First of all make sure to have the latest version of `pip <https://pypi.python.org/pypi/pip>`_ installed

.. prompt:: bash

   pip install --upgrade pip

The package and its python dependencies can be installed running the commands:

.. prompt:: bash

   pip install --upgrade numpy
   pip install --upgrade semilattices

.. toggle-header:: rubric
   :header: Manual installation

     If anything goes wrong with the automatic installation you can try to install from source:

     .. prompt:: bash
                 
        git clone git@bitbucket.org:joshuawchen/semilattices.git
        cd semilattices
        pip install --upgrade numpy
        pip install --upgrade -r requirements.txt
        python setup.py install

Running the Unit Tests
----------------------

Unit tests are available and can be run through the command:

.. prompt:: bash

   python -m unittest
