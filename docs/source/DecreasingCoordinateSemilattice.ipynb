{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import semilattices as SL\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Decreasing semilattices\n",
    "\n",
    "A decreasing (lower, down) semilattice is a [coordinate semilattice](CoordinateSemilattice.html) which fulfill the following **decreasing property**:\n",
    "\n",
    "* every vertex $v\\in\\mathcal{V}$ has a **complete parenthood**, i.e. the semilattice contains all the possible parents of $v$.\n",
    "\n",
    "The number of possible parents for a vertex $v$ is equivalent to the number $n$ of its non-zero coordinates (i.e. $n=\\#\\{c\\in v.\\text{coordinates} | c \\neq 0\\}$). Some communities denote decreasing semilattices as **downward closed** semilattices.\n",
    "\n",
    "The construction of decreasing semilattices is equivalent to the construction of coordinate semilattices, apart from the fact that one is not allowed to insert vertices violating the decreasing property. A decreasing semilattice exploits this property to efficiently provide the user with the possible children that can be added to the set semilattice while maintaining the decreasing property."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl = SL.DecreasingCoordinateSemilattice(dims=2)\n",
    "root = sl.new_vertex()\n",
    "c0 = sl.new_vertex(edge=0, parent=root)\n",
    "c1 = sl.new_vertex(edge=1, parent=root)\n",
    "c0c0 = sl.new_vertex(edge=0, parent=c0)\n",
    "c0c1 = sl.new_vertex(edge=1, parent=c0)\n",
    "c1c1 = sl.new_vertex(edge=1, parent=c1)\n",
    "c0c1c1 = sl.new_vertex(edge=1, parent=c0c1)\n",
    "sl.draw(cartesian=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The admissible frontier\n",
    "\n",
    "A fudamental concept in decreasing semilattices is the admissible frontier, which is the set of vertices $\\mathcal{A}\\subset\\mathcal{F}\\subset\\mathcal{V}$ such that $v\\in\\mathcal{A}$ have less than $d$ children and could insert at least one child which does not violate the decreasing property.\n",
    "\n",
    "In other words, the admissible frontier $\\mathcal{A}$ contains vertices to which child(ren) may be added without violating the **decreasing property** (or child(ren) which are **admissible**).\n",
    "\n",
    "In some communities, the $\\mathcal{A}$ is referred to as the **active set**, and $\\mathcal{F} \\setminus \\mathcal{A}$ is referred to as the **old set**.\n",
    "\n",
    "The admissible frontier can be accessed through the following data structure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.admissible_frontier"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also visualize the nodes in the admissible frontier through arrows. Vertices in the admissible frontier are marked with arrows indicating the directions in which children can be added"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, mark_admissible_frontier=True, mark_childless=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's delete a vertex:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.delete_vertex(c1c1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, mark_admissible_frontier=True, mark_childless=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's look at the childless vertices (♢)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in sl.childless:\n",
    "    print(v.coordinates)\n",
    "print(\"or another way:\")\n",
    "for v in SL.LevelsIterable(sl,iter_attribute=\"_l1_childless_partition\"):\n",
    "    print(v.coordinates)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for lvl, vertices in sl.l1_childless_partition.items():\n",
    "    print(\"lvl\",lvl,\":\")\n",
    "    for v in vertices:\n",
    "        print(v.coordinates)\n",
    "    print(\"\\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The visualization also shows the directions toward which children may be added to vertices in the admissible frontier. This information may be accessed through the [sparse_keys](api-semilattices.html#SparseVertex.sparse_keys) propery of vertices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "v = next(iter(sl.admissible_frontier))\n",
    "print(v.coordinates)\n",
    "print(v.sparse_keys)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The admissible frontier can be accessed also through its $\\ell^1$ partition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.l1_admissible_frontier_partition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This partition can be iterated over using the [LevelsIterable](api-semilattices.html#LevelsIterable) class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in SL.LevelsIterable(sl, iter_attribute='l1_admissible_frontier_partition'):\n",
    "    print(v.coordinates)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To illustrate further how vertices become admissible as the semilattice evolves, let us now try to insert a vertex violating the decreasing property."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    c0c1c1 = sl.new_vertex(edge=1, parent=c0c1)\n",
    "except SL.ViolatesDecreasingProperty:\n",
    "    print(\"The new vertex violates the decreasing property.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Adding the vertex c1c1 = {1:2} causes vertex {0: 1, 1:2} to be come admissible. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c1c1 = sl.new_vertex(edge=1, parent=c1)\n",
    "sl.draw(cartesian=True, mark_childless=True, mark_admissible_frontier=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, inserting the same vertex no longer violates the decreasing property"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    c0c1c1 = sl.new_vertex(edge=1, parent=c0c1)\n",
    "    sl.draw(cartesian=True, mark_childless=True, mark_admissible_frontier=True, show=False)\n",
    "except SL.ViolatesDecreasingProperty:\n",
    "    print(\"The new vertex violates the decreasing property.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A convenience function allows the user to add all children that would maintain the decreasing property of the semilattice. Below we show a sequence of adding all admissible children of vertices in the current admissible frontier"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for v in list(sl.admissible_frontier):\n",
    "    sl.add_all_admissible_children_of(v)\n",
    "    sl.draw(cartesian=True, mark_admissible_frontier=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creation of $\\ell^p$-balls\n",
    "\n",
    "For $p \\in [0,\\infty]$, an $\\ell^p$ ball semilattice with radius $r \\geq 0$ is a coordinate semilattice such that:\n",
    "\n",
    "* the coordinates ${\\bf c} = v.\\text{coordinates}$ of every vertex $v$ in the semilattice satisfy $\\Vert{\\bf c}\\Vert_p \\leq r$,\n",
    "\n",
    "where $\\Vert {\\bf c} \\Vert_p := \\left( \\sum_{i=0}^{d-1} {\\bf c}_i^p \\right)^{1/p}$ for $p<\\infty$, $\\Vert {\\bf c} \\Vert_\\infty := \\max_i {\\bf c_i}$, and $\\Vert {\\bf c} \\Vert_0 := \\sum_i^{d-1} {\\bf c}_i$ if $\\#\\{{\\bf c}_i \\ne 0\\} = 1$, else $\\infty$ (our own definition)\n",
    "\n",
    "Semilattices fulfilling this condition, fulfill automatically also the *decreasing property*.\n",
    "\n",
    "We can create these decreasing semilattices using the following function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl = SL.create_lp_semilattice(dims=2, norm=10, p=0.6)\n",
    "sl.draw(cartesian=True, mark_frontier=False, mark_childless=True, mark_admissible_frontier=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To illustrate deletion of vertices, we will draw a random vertex and delete it:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "delete_vertex = sl.random_vertex()\n",
    "print(\"Deleting this vertex:\", delete_vertex.coordinates)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.delete_vertex(delete_vertex)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl.draw(cartesian=True, mark_frontier=True, mark_childless=True, mark_admissible_frontier=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that deleting a vertex will also result in deleting all vertices in the semilattice that violate the decreasing property as a result of the deletion."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Below we see a contrast between semilattice that are ``sparse`` and ones which are not. We create $\\ell^0$-ball and a $\\ell^{\\infty}$ ball semilattices:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl = SL.create_lp_semilattice(dims=2, norm=10, p=0)\n",
    "sl.draw(cartesian=True, mark_childless=True, mark_admissible_frontier=True, show=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl = SL.create_lp_semilattice(dims=2, norm=10, p=np.inf)\n",
    "sl.draw(cartesian=True, mark_childless=True, mark_admissible_frontier=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For convenience, a method ``sl.random_vertex`` gives one the ability to access random elements of the semilattice. We use this to delete a random vertex."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "delete_vertex = sl.random_vertex()\n",
    "print(\"Deleting this vertex:\", delete_vertex.coordinates)\n",
    "sl.delete_vertex(delete_vertex)\n",
    "sl.draw(cartesian=True, mark_frontier=True, mark_childless=True, mark_admissible_frontier=True, show=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Different coordinates can also be assigned different weights, leading to the **decreasing semilattices within weighted $\\ell^p_{\\bf w}$-balls** of a certain radius, where the weghted norm (quasinorm) is defined by $\\Vert{\\bf c}\\Vert_{p,{\\bf w}} := \\left( \\sum_{i=0}^{d-1} {\\bf w}_i {\\bf c}_i^p \\right)^{1/p}$ for $p<\\infty$ and $\\Vert{\\bf c}\\Vert_{\\infty,{\\bf w}} := {\\bf w}_{\\hat{i}} {\\bf c}_\\hat{i}$ otherwise ($\\hat{i}:=\\arg\\max_i{\\bf c}_i$)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sl = SL.create_lp_semilattice(dims=2, norm=45, p=0.5, weights={0: 1., 1:2.0})\n",
    "sl.draw(cartesian=True, mark_frontier=False, mark_childless=False, mark_admissible_frontier=True, show=False, marker_size=10.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "delete_vertex = sl.random_vertex()\n",
    "print(\"Deleting this vertex:\", delete_vertex.coordinates)\n",
    "sl.delete_vertex(delete_vertex)\n",
    "sl.draw(cartesian=True, mark_frontier=True, mark_childless=True, mark_admissible_frontier=True, show=False, marker_size=20.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This way we can quickly generate large isotropic or anisotropic decreasing semilattices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dims = 50\n",
    "sl = SL.create_lp_semilattice(\n",
    "    dims=dims, norm=10, p=0.8, \n",
    "    weights=dict([ (d, np.sqrt(d+1)) for d in range(dims) ]) )\n",
    "sl.draw(show=False)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
