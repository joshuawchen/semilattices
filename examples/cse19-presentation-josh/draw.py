import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import semilattices as SL

def plot(sl):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.view_init(30, 35)
    fig = sl.draw(ax=ax, cartesian=True, show=False)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_zticks([])
    plt.show(False)
    return fig

sl1 = SL.DecreasingCoordinateSemilattice(dims=3)
v = sl1.new_vertex()
for i in range(4):
    v = sl1.new_vertex(
        parent = v,
        edge   = 0
    )
plot(sl1)

sl2 = SL.DecreasingCoordinateSemilattice(dims=3)
root = sl2.new_vertex()
v = root
for i in range(2):
    v = sl2.new_vertex(
        parent = v,
        edge   = 0
    )
v = root
for i in range(3):
    v = sl2.new_vertex(
        parent = v,
        edge   = 1
    )
sl2.new_vertex(
    parent = root.children[0],
    edge=1 )
sl2.new_vertex(
    parent = root.children[0].children[0],
    edge=1 )
sl2.new_vertex(
    parent = root.children[0].children[1],
    edge=1 )
plot(sl2)

sl3 = SL.DecreasingCoordinateSemilattice(dims=3)
root = sl3.new_vertex()
v = root
for i in range(4):
    v = sl3.new_vertex(
        parent = v,
        edge   = 1
    )
v = root
for i in range(3):
    v = sl3.new_vertex(
        parent = v,
        edge   = 2
    )
sl3.new_vertex(
    parent = root.children[1],
    edge = 2 )
sl3.new_vertex(
    parent = root.children[1].children[1],
    edge = 2 )
sl3.new_vertex(
    parent = root.children[2].children[1],
    edge = 2 )
plot(sl3)

# Take the union
sl = sl1 | sl2 | sl3
plot(sl)
