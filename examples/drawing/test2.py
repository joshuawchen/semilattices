from math import inf
import numpy as np
import semilattices as SL

opts = {
    'color': 'label',
    'mark_frontier': True,
    'mark_admissible_frontier': True,
    'nodes_cmap': 'jet'
}

opts_cart = opts.copy()
opts_cart['cartesian'] = True

p = .4

sl = SL.create_lp_semilattice(
    2, 30, p=1.,
    SemilatticeConstructor=SL.SortedDecreasingCoordinateSemilattice,
    sorting_labels = ['weight']
)
for v in sl: sl.update_labels( v, weight=v.coordinates.lp(p=p) )
opts['color_label'] = 'weight'
opts_cart['color_label'] = 'weight'
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(
    2, 30, p=.5,
    SemilatticeConstructor=SL.SortedDecreasingCoordinateSemilattice,
    sorting_labels = ['weight', 'random'])
for v in sl: sl.update_labels(
        v,
        weight=v.coordinates.lp(p=p),
        random=np.random.randn())
sl.draw(**opts)
sl.draw(**opts_cart)

opts['color_label'] = 'random'
opts_cart['color_label'] = 'random'
sl.draw(**opts)
sl.draw(**opts_cart)
opts['color_label'] = 'weight'
opts_cart['color_label'] = 'weight'

sl = SL.create_lp_semilattice(
    2, 5, p=inf,
    SemilatticeConstructor=SL.SortedDecreasingCoordinateSemilattice,
    sorting_labels = ['weight'])
for v in sl: sl.update_labels( v, weight=v.coordinates.lp(p=p) )
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(
    2, 30, p=.6, weights={0:1., 1:1.5},
    SemilatticeConstructor=SL.SortedDecreasingCoordinateSemilattice,
    sorting_labels = ['weight'])
for v in sl: sl.update_labels( v, weight=v.coordinates.lp(p=p) )
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(
    3, 10, p=1.,
    SemilatticeConstructor=SL.SortedDecreasingCoordinateSemilattice,
    sorting_labels = ['weight'])
for v in sl: sl.update_labels( v, weight=v.coordinates.lp(p=p) )
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(
    3, 10, p=.5,
    SemilatticeConstructor=SL.SortedDecreasingCoordinateSemilattice,
    sorting_labels = ['weight'])
for v in sl: sl.update_labels( v, weight=v.coordinates.lp(p=p) )
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(
    3, 5, p=inf,
    SemilatticeConstructor=SL.SortedDecreasingCoordinateSemilattice,
    sorting_labels = ['weight'])
for v in sl: sl.update_labels( v, weight=v.coordinates.lp(p=p) )
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(
    3, 10, p=.6, weights={0:1., 1:1.5, 2:2.},
    SemilatticeConstructor=SL.SortedDecreasingCoordinateSemilattice,
    sorting_labels = ['weight'])
for v in sl: sl.update_labels( v, weight=v.coordinates.lp(p=p) )
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(
    5, 30, p=.6, weights={0:1., 1:1.1, 2:1.5, 3:2., 4:4.},
    SemilatticeConstructor=SL.SortedDecreasingCoordinateSemilattice,
    sorting_labels = ['weight'])
for v in sl: sl.update_labels( v, weight=v.coordinates.lp(p=p) )
sl.draw(**opts)
