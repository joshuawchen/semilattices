from math import inf
import numpy as np
import semilattices as SL

opts = {
    'mark_frontier': True,
    'mark_admissible_frontier': True,
}

opts_cart = opts.copy()
opts_cart['cartesian'] = True

sl = SL.create_lp_semilattice(2, 30, p=1.)
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(2, 30, p=.5)
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(2, 5, p=inf)
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(2, 5, p=0.001)
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(2, 30, p=.6, weights={0:1., 1:1.5})
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(3, 10, p=1.)
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(3, 10, p=.5)
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(3, 5, p=inf)
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(3, 10, p=.6, weights={0:1., 1:1.5, 2:2.})
sl.draw(**opts)
sl.draw(**opts_cart)

sl = SL.create_lp_semilattice(5, 30, p=.6, weights={0:1., 1:1.1, 2:1.5, 3:2., 4:4.})
sl.draw(**opts)
